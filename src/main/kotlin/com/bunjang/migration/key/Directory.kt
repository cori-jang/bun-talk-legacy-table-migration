package com.bunjang.migration.key

import com.bunjang.migration.jpa.entity.S3Talk1Message
import com.bunjang.migration.jpa.entity.Talk1Message
import com.bunjang.migration.toJson
import com.bunjang.migration.toUTC
import java.time.LocalDateTime

class Directory(
    talk1Message: Talk1Message
) {
    private val year: Int
    private val month: Int
    private val day: Int
    private val hour: Int
    val fileName: String
    val filePath: String

    init {
        val dateTime = talk1Message.updateTime
        val utcDateTime = dateTime.toUTC().toLocalDateTime()
        year = utcDateTime.year
        month = utcDateTime.monthValue
        day = utcDateTime.dayOfMonth
        hour = utcDateTime.hour

        fileName = String.format(
            "bun-log-buntalk-legacy-${utcDateTime.year}-%02d-%02d-%02d-%02d-%02d",
            utcDateTime.monthValue,
            utcDateTime.dayOfMonth,
            utcDateTime.hour,
            utcDateTime.minute,
            utcDateTime.second
        ) + "-" + talk1Message.channelId + "-" + talk1Message.messageNo + ".gz"

        filePath = String.format("buntalk-legacy/year=$year/month=%02d/day=%02d/hour=%02d", month, day, hour) +
                "/" + fileName
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Directory

        if (year != other.year) return false
        if (month != other.month) return false
        if (day != other.day) return false
        if (hour != other.hour) return false

        return true
    }

    override fun hashCode(): Int {
        var result = year
        result = 31 * result + month
        result = 31 * result + day
        return result
    }
}

class PreparedFile(
    private val jsonContents: StringBuilder = java.lang.StringBuilder(),
    var rawCount: Int = 0
) {
    fun addJsonMessage(s3Talk1Message: S3Talk1Message) {
        if (rawCount > 0) jsonContents.append("\n")
        jsonContents.append(s3Talk1Message.toJson())
        rawCount++
    }

    val content: String
        get() = jsonContents.toString()
}

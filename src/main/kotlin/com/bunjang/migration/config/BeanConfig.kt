package com.bunjang.migration.config

import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.ComponentScans
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScans(
    ComponentScan("com.bunjang.migration"),
)
class PackageConfig

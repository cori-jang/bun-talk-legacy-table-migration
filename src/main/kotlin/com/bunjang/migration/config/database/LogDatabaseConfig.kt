package com.bunjang.migration.config.database

import com.bunjang.jpa.JPAScheduler
import com.bunjang.jpa.scheduler
import com.bunjang.jpa.support.ScheduledJpaRepositoryFactoryBean
import com.zaxxer.hikari.HikariDataSource
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.jdbc.datasource.LazyConnectionDataSourceProxy
import org.springframework.orm.jpa.JpaTransactionManager
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean
import org.springframework.transaction.PlatformTransactionManager
import org.springframework.transaction.annotation.EnableTransactionManagement
import javax.persistence.EntityManagerFactory
import javax.sql.DataSource

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
    basePackages = ["com.bunjang.migration.jpa.repository"],
    entityManagerFactoryRef = "logEntityManager",
    transactionManagerRef = "logTransactionManager",
    repositoryFactoryBeanClass = ScheduledJpaRepositoryFactoryBean::class
)
class LogDatabaseConfig {

    @Bean
    @Primary
    @ConfigurationProperties(prefix = "log.master.datasource")
    fun logMasterDataSourceProperties(): DataSourceProperties {
        return DataSourceProperties()
    }

    @Bean
    @Primary
    @ConfigurationProperties(prefix = "log.master.datasource.hikari")
    fun logMasterHikariDataSource(@Qualifier("logMasterDataSourceProperties") masterProperty: DataSourceProperties): HikariDataSource {
        return masterProperty.initializeDataSourceBuilder().type(HikariDataSource::class.java).build()
    }

    @Bean
    @ConfigurationProperties(prefix = "log.slave.datasource")
    fun logSlaveDataSourceProperties(): DataSourceProperties {
        return DataSourceProperties()
    }

    @Bean
    @ConfigurationProperties(prefix = "log.slave.datasource.hikari")
    fun logSlaveHikariDataSource(@Qualifier("logSlaveDataSourceProperties") slaveProperty: DataSourceProperties): HikariDataSource {
        return slaveProperty.initializeDataSourceBuilder().type(HikariDataSource::class.java).build()
    }

    @Bean
    fun logRoutingDataSource(
        @Qualifier("logMasterHikariDataSource") masterDataSource: DataSource,
        @Qualifier("logSlaveHikariDataSource") slaveDataSource: DataSource
    ): DataSource {
        val dataSourceMap: Map<Any, Any> = hashMapOf(
            DBType.MASTER to masterDataSource,
            DBType.SLAVE to slaveDataSource
        )

        return LazyConnectionDataSourceProxy(MasterSlaveRoutingDataSource().apply {
            this.setDefaultTargetDataSource(masterDataSource)
            this.setTargetDataSources(dataSourceMap)
            this.afterPropertiesSet()
        })
    }

    @Bean
    @Primary
    @ConfigurationProperties(prefix = "log.jpa")
    fun logJpaProperties(): JpaProperties {
        return JpaProperties()
    }

    @Bean
    @Primary
    fun logEntityManager(
        builder: EntityManagerFactoryBuilder,
        @Qualifier("logRoutingDataSource") logRoutingDataSource: DataSource,
        logJPAScheduler: JPAScheduler
    ): LocalContainerEntityManagerFactoryBean {
        return builder
            .dataSource(logRoutingDataSource)
            .packages("com.bunjang.migration.jpa.entity")
            .scheduler(logJPAScheduler)
            .build()
    }

    @Bean
    @Primary
    fun logTransactionManager(logEntityManager: EntityManagerFactory): PlatformTransactionManager {
        return JpaTransactionManager(logEntityManager)
    }

    @Bean
    @Primary
    fun logJPAScheduler(
        @Value("\${log.master.datasource.hikari.maximum-pool-size}") masterMaxPoolSize: Int,
        @Value("\${log.slave.datasource.hikari.maximum-pool-size}") slaveMaxPoolSize: Int,
        @Value("\${log.maximum-jdbc-thread-pool-size}") poolSize: Int? = null
    ): JPAScheduler {
        val maxPoolSize = poolSize ?: (masterMaxPoolSize + slaveMaxPoolSize)
        return JPAScheduler.of(maxPoolSize, "logJdbcScheduler")
    }
}

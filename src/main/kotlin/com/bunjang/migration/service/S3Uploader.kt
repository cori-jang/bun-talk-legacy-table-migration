package com.bunjang.migration.service

import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.CannedAccessControlList
import com.amazonaws.services.s3.model.PutObjectRequest
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import java.io.File
import java.io.FileOutputStream

@Service
class S3Uploader(
    private val amazonS3Client: AmazonS3
) {
    @Value("\${cloud.aws.s3.bucket}")
    private lateinit var bucket: String

//    fun upload(multipartFile: MultipartFile, dirName: String): String {
//        val uploadFile: File = convert(multipartFile)
//        return upload(uploadFile, dirName)
//    }

//    fun upload(uploadFile: File, dirName: String): String {
//        val fileName = dirName + "/" + uploadFile.name
//        return putS3(fileName, uploadFile)
//    }

    fun putS3(fileName: String, uploadFile: File): String {
        amazonS3Client.putObject(
            PutObjectRequest(
                bucket,
                fileName,
                uploadFile
            ).withCannedAcl(CannedAccessControlList.Private)
        )
        return amazonS3Client.getUrl(bucket, fileName).toString()
    }

//    private fun convert(multipartFile: MultipartFile): File {
//        val convertFile: File = File(multipartFile.originalFilename!!)
//        if (convertFile.createNewFile()) {
//            try {
//                FileOutputStream(convertFile).use { fos ->
//                    fos.write(multipartFile.bytes)
//                }
//            } catch (e: Exception) {
//                throw RuntimeException("파일 쓰기 실패", e)
//            }
//        } else throw RuntimeException("createNewFile 실패")
//
//        return convertFile
//    }
}

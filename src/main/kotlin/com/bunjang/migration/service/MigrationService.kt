package com.bunjang.migration.service

import com.bunjang.migration.jpa.entity.S3Talk1Message
import com.bunjang.migration.jpa.repository.Talk1MessageRepository
import com.bunjang.migration.key.Directory
import com.bunjang.migration.key.PreparedFile
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.core.io.ClassPathResource
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Service
import org.springframework.util.StopWatch
import java.io.*
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.zip.GZIPOutputStream

@Service
class MigrationService(
    private val s3Uploader: S3Uploader,
    private val talk1MessageRepository: Talk1MessageRepository
) {

    val logger: Logger =
        LoggerFactory.getLogger(MigrationService::class.java)

    companion object {
        const val LOOK_UP_MAX_SIZE = 1000
        const val MESSAGE_JSON_MAX_RAW_SIZE =
            1024 * 10 // message 하나 당 약 0.3kb 이므로 1024 * 10 * 0.3kb = 3mb 이므로 이 정도에서 압축하자
    }

    //    fun test() {
//        val resource = ClassPathResource("data/test")
//        val file = resource.file
//        s3Uploader.upload(file, "buntalk-legacy")
//    }
    fun migrationMonthly(startDate: LocalDate, endDate: LocalDate): Long {
        val stopWatch1 = StopWatch()
        stopWatch1.start()
        logger.info("migrationMonthly start : startDate[{}] <= ~ < endDate[{}]", startDate, endDate)

        var migratedMessageTotalCount = 0L

        var currentDate = startDate.withDayOfMonth(1)
        val limitDate = endDate.withDayOfMonth(1)

        while (currentDate.isBefore(limitDate)) {
            val untilDate = currentDate.plusMonths(1)
            migratedMessageTotalCount += migrationDaily(currentDate, untilDate)
            currentDate = untilDate
        }

        stopWatch1.stop()
        logger.info(
            "migrationMonthly end : startDate[{}] <= ~ < endDate[{}] -> response_time=[{}], totalCount={}",
            startDate,
            endDate,
            prettyTime(stopWatch1.lastTaskTimeMillis),
            migratedMessageTotalCount
        )
        return migratedMessageTotalCount
    }

    fun migrationDaily(startDate: LocalDate, endDate: LocalDate): Long {
        val stopWatch1 = StopWatch()
        stopWatch1.start()
        logger.info("migrationDaily start : startDate[{}] <= ~ < endDate[{}]", startDate, endDate)

        var currentDate: LocalDate = startDate
        var migratedMessageTotalCount = 0L

        val gapTime = 1L
        while (currentDate.isBefore(endDate)) {

            var migratedMessageTotalCountInDay = 0L
            var startDateTime: LocalDateTime = currentDate.atStartOfDay()
            val limitDateTime: LocalDateTime = currentDate.plusDays(1).atStartOfDay()

            val stopWatch2 = StopWatch()
            stopWatch2.start()
            while (startDateTime < limitDateTime) {
                val endDateTime = startDateTime.plusHours(gapTime)
                migratedMessageTotalCountInDay += migration(startDateTime, endDateTime)
                startDateTime = startDateTime.plusHours(gapTime)
            }

            stopWatch2.stop()
            logger.info(
                "migration end in day : targetDate[{}] -> response_time=[{}], totalCount={}",
                currentDate,
                prettyTime(stopWatch2.lastTaskTimeMillis),
                migratedMessageTotalCountInDay
            )
            migratedMessageTotalCount += migratedMessageTotalCountInDay
            currentDate = currentDate.plusDays(1)
        }

        stopWatch1.stop()
        logger.info(
            "migrationDaily end : startDate[{}] <= ~ < endDate[{}] -> response_time=[{}], totalCount={}",
            startDate,
            endDate,
            prettyTime(stopWatch1.lastTaskTimeMillis),
            migratedMessageTotalCount
        )
        return migratedMessageTotalCount
    }

    fun migration(startDateTime: LocalDateTime, endDateTime: LocalDateTime): Long {
        var migratedMessageTotalCount = 0L
        var page = 0
        val fileMap = mutableMapOf<Directory, PreparedFile>()

        val messageList = talk1MessageRepository.findAllUpdateTimeBetween(
            startDateTime,
            endDateTime,
            PageRequest.of(0, 1, Sort.Direction.ASC, "updateTime", "channelId", "messageNo")
        )

        if (messageList.isEmpty()) return 0
        val firstMessage = messageList[0]

        var currentTargetDateTime = firstMessage.updateTime
        var currentDirectory = Directory(firstMessage)

        while (true) {
            val talk1MessageList = talk1MessageRepository.findAllUpdateTimeBetween(
                startDateTime,
                endDateTime,
                PageRequest.of(page, LOOK_UP_MAX_SIZE, Sort.Direction.ASC, "updateTime", "channelId", "messageNo")
            )
            if (talk1MessageList.isEmpty()) break

            for (talk1Message in talk1MessageList) {

                val s3talk1Message = S3Talk1Message.of(talk1Message)

                if (currentTargetDateTime.hour != talk1Message.updateTime.hour) {
                    val preparedFile = fileMap[currentDirectory]

                    if (preparedFile != null) {
                        createFilePutS3DeleteMapKey(currentDirectory, preparedFile, fileMap)
                        migratedMessageTotalCount += preparedFile.rawCount
                    }

                    currentTargetDateTime = talk1Message.updateTime
                    currentDirectory = Directory(talk1Message)
                    fileMap[currentDirectory] = PreparedFile().apply { addJsonMessage(s3talk1Message) }
                    continue
                }

                var preparedFile = fileMap[currentDirectory]

                if (preparedFile == null) {
                    currentTargetDateTime = talk1Message.updateTime
                    currentDirectory = Directory(talk1Message)
                    preparedFile = PreparedFile()
                    fileMap[currentDirectory] = preparedFile
                }

                preparedFile.addJsonMessage(s3talk1Message)
                if (preparedFile.rawCount >= MESSAGE_JSON_MAX_RAW_SIZE) {
                    createFilePutS3DeleteMapKey(currentDirectory, preparedFile, fileMap)
                    migratedMessageTotalCount += preparedFile.rawCount
                }
            }

            page++
        }

        val preparedFile = fileMap[currentDirectory]
        if (preparedFile != null) {
            createFilePutS3DeleteMapKey(currentDirectory, preparedFile, fileMap)
            migratedMessageTotalCount += preparedFile.rawCount
        }

        return migratedMessageTotalCount
    }

    private fun createFilePutS3DeleteMapKey(
        currentDirectory: Directory,
        preparedFile: PreparedFile,
        fileMap: MutableMap<Directory, PreparedFile>
    ) {
        val createdFile = createZipFile(currentDirectory.fileName, preparedFile.content)
        s3Uploader.putS3(currentDirectory.filePath, createdFile)
        fileMap.remove(currentDirectory)
    }

    private fun createZipFile(fileName: String, content: String): File {
        val fileLocation = File("src/main/resources/").absolutePath + "/" + fileName

        try {
            FileOutputStream(fileLocation).use { fos ->
                GZIPOutputStream(fos).use { gos ->
                    BufferedOutputStream(gos).use { bos ->
                        OutputStreamWriter(bos).use { writer ->
                            writer.write(content)
                        }
                    }
                }
            }
        } catch (e: Exception) {
            logger.error("fileName : {} 에 파일 생성 실패했습니다.", fileName)
            throw e
        }

        return File(fileLocation)
    }

    private fun createFile(filePath: String, content: String): File {
        val fileLocation = File("src/main/resources/").absolutePath + "/" + filePath;

        try {
            FileWriter(fileLocation, false).use { fw ->
                BufferedWriter(fw).use { writer ->
                    writer.write(content)
                }
            }
        } catch (e: Exception) {
            logger.error("filePath : {} 에 파일 생성 실패했습니다.", filePath)
            throw e
        }

        return File(fileLocation)
    }

    private fun prettyTime(ms: Long): String {
        val secs = ms / 1000
        val sec = secs % 60
        val min = secs / 60 % 60
        val hour = secs / 3600 % 24
        val day = (secs / (3600 * 24))

        return String.format("day: %d, hour: %02d, minute: %02d, second: %02d", day, hour, min, sec)
    }
}

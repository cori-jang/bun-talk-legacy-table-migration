package com.bunjang.migration.rest.resource

import com.bunjang.migration.jpa.entity.Talk1MessageId
import com.bunjang.migration.jpa.repository.Talk1MessageRepository
import com.bunjang.migration.service.MigrationService
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class TestResource(
    private val talk1MessageRepository: Talk1MessageRepository,
    private val migrationService: MigrationService
) {
    @GetMapping("/test")
    fun test() = talk1MessageRepository.findAllBy(PageRequest.of(0, 10, Sort.Direction.ASC, "updateTime"))

    @GetMapping("/test2")
    fun test2() = talk1MessageRepository.findById(Talk1MessageId(channelId = 34668801, messageNo = 4))

//    @GetMapping("/test3")
//    fun test3() = migrationService.test()
}

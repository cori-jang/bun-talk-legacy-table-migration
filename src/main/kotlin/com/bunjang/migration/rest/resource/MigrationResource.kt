package com.bunjang.migration.rest.resource

import com.bunjang.migration.service.MigrationService
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import java.time.LocalDate
import java.time.LocalDateTime

@RestController
class MigrationResource(
    private val migrationService: MigrationService
) {
    @GetMapping("/monthly/migration")
    fun migrationMonthly(
        @DateTimeFormat(pattern = "yyyy-MM-dd")
        startDate: LocalDate,
        @DateTimeFormat(pattern = "yyyy-MM-dd")
        endDate: LocalDate
    ) = migrationService.migrationMonthly(startDate, endDate)

    @GetMapping("/daily/migration")
    fun migrationDaily(
        @DateTimeFormat(pattern = "yyyy-MM-dd")
        startDate: LocalDate,
        @DateTimeFormat(pattern = "yyyy-MM-dd")
        endDate: LocalDate
    ) = migrationService.migrationDaily(startDate, endDate)
}

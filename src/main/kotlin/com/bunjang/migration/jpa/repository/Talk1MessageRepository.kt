package com.bunjang.migration.jpa.repository

import com.bunjang.jpa.ScheduledJpaRepository
import com.bunjang.migration.jpa.entity.Talk1Message
import com.bunjang.migration.jpa.entity.Talk1MessageId
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Slice
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import java.time.LocalDateTime

@Repository
interface Talk1MessageRepository : ScheduledJpaRepository<Talk1Message, Talk1MessageId> {

    @Transactional(readOnly = true)
    fun findAllBy(pageable: Pageable): List<Talk1Message>

    @Transactional(readOnly = true)
    @Query(
        """
            SELECT msg
            FROM Talk1Message msg
            WHERE msg.updateTime >= :startDateTime
            AND   msg.updateTime < :endDateTime
        """
    )
    fun findAllUpdateTimeBetween(
        startDateTime: LocalDateTime,
        endDateTime: LocalDateTime,
        pageable: Pageable
    ): List<Talk1Message>
}

package com.bunjang.migration.jpa.entity

import com.bunjang.migration.toUTC
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.io.Serializable
import javax.persistence.*

@EntityListeners(AuditingEntityListener::class)
@Entity
@Table(name = "tb_message")
@IdClass(Talk1MessageId::class)
class Talk1Message(

    @Id
    @Column(name = "channel_id")
    val channelId: Long,

    @Id
    @Column(name = "message_no")
    val messageNo: Int,

    @Column(name = "typecode")
    val typeCode: Int,

    @Column(name = "writer_id")
    val writerId: Int,

    @Column(name = "message_id")
    val messageId: Long,

    @Column(name = "content")
    val content: String,

    @Column(name = "extras")
    val extras: String?
) {
    @CreatedDate
    @Column(name = "create_time", nullable = false)
    lateinit var createTime: java.time.LocalDateTime

    @LastModifiedDate
    @Column(name = "update_time", nullable = false)
    lateinit var updateTime: java.time.LocalDateTime
}

data class Talk1MessageId(
    val channelId: Long = 0L,
    val messageNo: Int = 0,
) : Serializable

class S3Talk1Message(
    val buntalk1_channel_id: String,
    val buntalk1_message_no: String,
    val message_type: String,
    val uid: String,
    val buntalk1_message_id: String,
    val created_at: String,
    //val updated_at: String,
    val content: String,
    val extra: String?
) {
    companion object {
        fun of(talk1Message: Talk1Message) = S3Talk1Message(
            buntalk1_channel_id = talk1Message.channelId.toString(),
            buntalk1_message_no = talk1Message.messageNo.toString(),
            message_type = talk1Message.typeCode.toString(),
            uid = talk1Message.writerId.toString(),
            buntalk1_message_id = talk1Message.messageId.toString(),
            created_at = talk1Message.createTime.toUTC().toLocalDateTime().toString(),
            //updated_at = talk1Message.updateTime.toUTC().toLocalDateTime().toString(),
            content = talk1Message.content,
            extra = talk1Message.extras
        )
    }
}

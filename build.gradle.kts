import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    val kotlinVersion = "1.4.32"

    id("org.springframework.boot") version "2.5.5"
    id("io.spring.dependency-management") version "1.0.11.RELEASE"
    kotlin("jvm") version kotlinVersion
    kotlin("plugin.spring") version kotlinVersion
    kotlin("plugin.jpa") version kotlinVersion
    kotlin("plugin.allopen") version kotlinVersion

}

allOpen {
    annotation("javax.persistence.Entity")
    annotation("javax.persistence.MappedSuperclass")
    annotation("javax.persistence.Embeddable")
}

group = "com.bunjang"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_1_8

repositories {
    mavenCentral()
    bunjangReleases()
    bunjangSnapshots()
    maven(url = "https://s3.amazonaws.com/redshift-maven-repository/release")
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.springframework.boot:spring-boot-starter-webflux")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("io.projectreactor.kotlin:reactor-kotlin-extensions")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor")

    implementation("com.amazonaws:aws-java-sdk-s3:1.11.847")

    implementation("com.bunjang:bun-webflux-logger:0.3.2")
    implementation("com.bunjang:bun-client-kotlin-starter:0.38")
    implementation("com.bunjang:bun-jpa-common:0.2.2")

    runtimeOnly("org.mariadb.jdbc:mariadb-java-client")

    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("io.projectreactor:reactor-test")
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "1.8"
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}

fun RepositoryHandler.bunjangReleases(): MavenArtifactRepository = bunjang("releases")
fun RepositoryHandler.bunjangSnapshots(): MavenArtifactRepository = bunjang("snapshots")

fun RepositoryHandler.bunjang(path: String): MavenArtifactRepository {
    return maven {
        setUrl("s3://bun-maven-repo.s3.ap-northeast-2.amazonaws.com/$path")
        authentication {
            create<AwsImAuthentication>("awsIm") // Load from EC2 role or env var. refer to DefaultAWSCredentialsProviderChain
        }
    }
}
